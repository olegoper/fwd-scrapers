import collections
from datetime import datetime
from bs4 import BeautifulSoup
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import resolve1
import pdftableextract
import requests


class LabeledValue(object):
    """Value wrapper with metadata."""

    def __init__(self, rate_type, crude_type, term, volume, from_name, from_value):
        self._rate_type = rate_type.decode('utf-8')
        self._crude_type = crude_type.decode('utf-8')
        self._term = term.decode('utf-8')
        self._volume = volume.decode('utf-8')
        self._from_name = from_name.decode('utf-8')
        self._from_value = from_value.decode('utf-8')

    def generate_fields(self):
        """
        Generates fields from metadata for given value.

        :return: dict with fields
        """
        return {
            'rate_type': self._rate_type,
            'crude_type': self._crude_type,
            'term': self._term,
            'volume': self._volume,
            'from_name': self._from_name,
            'from_value': self._from_value,
        }

    def generate_sid(self):
        """
        Generates sid from metadata.

        :return: SID value
        """
        return datastore.sid(self._rate_type, self._crude_type, self._term, self._volume, self._from_name)

    def __str__(self):
        return '{rate_type} {crude_type} {term} {volume} {from_name}'.format(
            rate_type=self._rate_type, crude_type=self._crude_type,
            term=self._term, volume=self._volume, from_name=self._from_name)


class TableParser(object):
    """Utility class for table parsing."""

    HEADER_PHRASE = 'SEASON COMMITTED [N] INTERNATIONAL JOINT RATES'
    RATE_CRUDE_HEADER_POSTFIX = 'TO ALL DELIVERY POINTS'
    NA_VALUE = 'N/A'

    def __init__(self):
        self._in_table = False
        self._debug_rows = []
        self._header = []
        self._data = []

        self._rate = None
        self._crude = None
        self._from_types = None

    def is_done(self):
        """
        Check if table parsing finished

        :return: True if table parsing finished
        :return: False otherwise
        """
        return not self._in_table and self._debug_rows

    def is_valid(self):
        """
        Check if a table was found

        :return: True if table was found
        :return: False otherwise
        """
        return bool(self._debug_rows)

    def parse_next_row(self, row):
        """
        Parse next row from a page.

        :param columns: row data
        """
        if not self._in_table:
            if self._is_header_start(row):
                self._in_table = True
            else:
                return
        else:
            if self._is_header_start(row):
                self._in_table = False
                return

        self._debug_rows.append(row)

        if len(self._header) < 4:
            self._header.append(row)
            if len(self._header) == 4:
                self._parse_header()
        else:
            self._parse_data(row)

    def make_labeled_data(self):
        """
        Generates data with labels from parsed table.

        :return: list with labeled data
        """
        res = []
        for term, volume, row in self._data:
            for name, value in row.iteritems():
                res.append(LabeledValue(
                        rate_type=self._rate, crude_type=self._crude,
                        term=term, volume=volume, from_name=name, from_value=value))
        return res

    @classmethod
    def _parse_rate_and_crude_types(cls, rows):
        assert len(rows) == 2, 'Bad rate and crude header length'
        text = ' '.join([it for row in rows for it in row if it])

        # Remove postfix
        text = text[:text.find(cls.RATE_CRUDE_HEADER_POSTFIX)].strip()

        words = text.split()
        split_index = next(i for i, it in enumerate(words) if it.startswith('BARREL'))
        words[split_index] = 'BARREL'
        split_index += 1
        rate = ' '.join(words[:split_index])
        crude = ' '.join(words[split_index:])

        return rate, crude

    @staticmethod
    def _parse_from_types(rows):
        assert len(rows) == 2, 'Bad from header length'
        assert all([rows[0][0] == 'Term', rows[0][1].startswith('Volume')]), 'Bad from header: ' + str(rows[0])

        return {i: column.strip() for i, column in enumerate(rows[1][2:])}

    def _parse_header(self):
        self._rate, self._crude = self._parse_rate_and_crude_types(self._header[:2])
        self._from_types = self._parse_from_types(self._header[2:])

    def _parse_data(self, row):
        data = {}
        term, volume = row[:2]
        for i, column in enumerate(row[2:]):
            label = self._from_types.get(i)
            value = column.strip()
            if not label or not value or value == self.NA_VALUE:
                continue
            data[label] = value.lstrip('$')

        if data:
            self._data.append([term, volume, data])

    def _is_header_start(self, row):
        return any(self.HEADER_PHRASE in it for it in row)


class PdfReader(object):
    """Utility class for PDF reading."""
    def __init__(self, pdf_path):
        self._pdf_path = pdf_path
        self._pages_count = None

    @property
    def pages_count(self):
        """Returns pages number in a file."""
        if self._pages_count is not None:
            return self._pages_count

        with open(self._pdf_path, 'rb') as f:
            parser = PDFParser(f)
            document = PDFDocument(parser)

            # This will give you the count of pages
            self._pages_count = resolve1(document.catalog['Pages'])['Count']
        return self._pages_count

    def read_all_pages(self):
        """
        Read PDF data.

        :return: list with rows
        """
        pages = [str(i + 1) for i in xrange(self.pages_count)]

        # Reduce bitmap_resolution for this kind of PDF
        def pepp(p):
            return pdftableextract.process_page(
                    self._pdf_path,
                    p,
                    bitmap_resolution=50,
            )

        cells = [pepp(p) for p in pages]

        #flatten the cells structure
        cells = [item for sublist in cells for item in sublist ]
        # return cells

        #without any options, process_page picks up a blank table at the top of the page.
        li = pdftableextract.table_to_list(cells, pages)
        return li


class PagesParser(object):
    """Parses multiple pages and populates corresponding tables."""
    def __init__(self):
        self.tables = []

    def parse_page(self, rows, page_num, pages_count):
        """
        Parses single page with tables.

        :param rows: page's rows
        :param page_num: page number
        :param pages_count: pages total in PDF
        """
        prev_tables_len = len(self.tables)
        table = TableParser()

        for row in rows:
            table.parse_next_row(row)
            if table.is_done():
                self.tables.append(table)

                table = TableParser()
                table.parse_next_row(row)

        if table.is_valid():
            self.tables.append(table)

        assert (len(self.tables) - prev_tables_len) in [0, 2], 'Bad tables count'


def download_pdf(url):
    """
    Downoads PDF document.

    :param url: URL with PDF document
    :return: PDF content
    """
    r = requests.get(url)
    assert r.status_code == 200, 'Bad status code {code}'.format(code=r.code)
    return r.content


def process_pdf_url(url):
    """
    Downloads, parses and submits data for given URL

    :param url: URL to process
    """
    logger.info('starting {}'.format(url))
    pdf_data = download_pdf(url)

    with tempfile.NamedTemporaryFile() as tf:
        tf.write(pdf_data)
        tf.flush()
        reader = PdfReader(tf.name)

        parser = PagesParser()
        pages = reader.read_all_pages()
        for page_num, page_rows in enumerate(pages):
            parser.parse_page(page_rows, page_num, len(pages))

    submit_tables(parser.tables)


def submit_tables(tables):
    """
    Submits tables data to Shooju.

    :param tables: tables with data to submit
    """
    sid_to_data = collections.defaultdict(list)
    for table in tables:
        for value in table.make_labeled_data():
            sid_to_data[value.generate_sid()].append(value)

    for sid, values in sid_to_data.iteritems():
        # should be unique for given series id
        fields = values[0].generate_fields()

        datastore.put_fields(sid, {'source_obj': fields})


def process_urls():
    """Entry point for URLs processing"""

    url = 'https://www.enbridge.com/~/media/Enb/Documents/Tariffs/2017/FSP_FERC_3150_EPI_NEB_425.pdf'
    process_pdf_url(url)
