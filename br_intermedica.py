import datetime
from StringIO import StringIO
import urlparse
from zipfile import ZipFile

from bs4 import BeautifulSoup
from dateutil.parser import parse
import requests


def _soup(content):
    return BeautifulSoup(content, "lxml")


class FormParser(object):
    def __init__(self, soup):
        self.soup = soup.find('form')

    def get_inputs(self):
        res = {}
        for it in self.soup.find_all('input'):
            res[it['name']] = it.get('value', '')
        return res

    def _get_select(self, name):
        return self.soup.find('select', {'name': name})

    def _get_options(self, name):
        return self._get_select(name).find_all('option')

    def get_selected_value(self, name):
        soup = self._get_select(name).find(selected=True)
        if soup is None:
            return ''
        return soup['value']

    def get_input_value(self, name):
        return self.get_inputs().get(name, '')

    def get_contrato_options(self):
        return self._get_options('ctl00$ContentPlaceHolder1$ddlContrato')

    def get_sub_contrato_options(self):
        return [
            it
            for it in self._get_options('ctl00$ContentPlaceHolder1$ddlSubContrato')
            if it.text != '(Todos)'
        ]

    def get_competencia_options(self):
        return self._get_options('ctl00$ContentPlaceHolder1$ddlCompetencia')


class FormPoster(FormParser):
    def __init__(self, session, soup, url):
        self._session = session
        self._url = url
        super(FormPoster, self).__init__(soup)

    def _post_form(self, form_data, raw=False):
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        r = self._session.post(self._url, data=form_data, headers=headers)
        r.raise_for_status()

        if raw:
            return r
        else:
            return _soup(r.content)

    def post_contrato(self, option):
        form_data = {
            '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$ddlContrato',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': option['value'],
            'ctl00$ContentPlaceHolder1$ddlSubContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlSubContrato'),
            'ctl00$ContentPlaceHolder1$ddlCompetencia': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlCompetencia'),
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        return self._post_form(form_data)

    def post_sub_contrato(self, option):
        form_data = {
            '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$ddlCompetencia',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlContrato'),
            'ctl00$ContentPlaceHolder1$ddlSubContrato': option['value'],
            'ctl00$ContentPlaceHolder1$ddlCompetencia': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlCompetencia'),
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        return self._post_form(form_data)

    def post_competencia(self, option):
        form_data = {
            '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$ddlSubContrato',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlContrato'),
            'ctl00$ContentPlaceHolder1$ddlSubContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlSubContrato'),
            'ctl00$ContentPlaceHolder1$ddlCompetencia': option['value'],
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        return self._post_form(form_data)

    def post_pesquisar(self):
        form_data = {
            '__EVENTTARGET': '',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlContrato'),
            'ctl00$ContentPlaceHolder1$ddlSubContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlSubContrato'),
            'ctl00$ContentPlaceHolder1$ddlCompetencia': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlCompetencia'),
            'ctl00$ContentPlaceHolder1$Button4': 'Pesquisar',
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        return self._post_form(form_data)

    def post_download_document(self, button, raw=False):
        form_data = {
            '__EVENTTARGET': '',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlContrato'),
            'ctl00$ContentPlaceHolder1$ddlSubContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlSubContrato'),
            'ctl00$ContentPlaceHolder1$ddlCompetencia': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlCompetencia'),
            '{}.x'.format(button): '5',
            '{}.y'.format(button): '10',
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        res = self._post_form(form_data, raw)
        if raw:
            return res.content
        return res

    def post_download_arquivos_texto(self):
        form_data = {
            '__EVENTTARGET': '',
            '__EVENTARGUMENT': '',
            '__LASTFOCUS': '',
            'ctl00$ContentPlaceHolder1$ddlContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlContrato'),
            'ctl00$ContentPlaceHolder1$ddlSubContrato': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlSubContrato'),
            'ctl00$ContentPlaceHolder1$ddlCompetencia': self.get_selected_value('ctl00$ContentPlaceHolder1$ddlCompetencia'),
            'ctl00$ContentPlaceHolder1$cmdGerar': 'Download Arquivos Texto',
            'ctl00$ContentPlaceHolder1$txtLinha': '',
            '__EVENTVALIDATION': self.get_input_value('__EVENTVALIDATION'),
            '__VIEWSTATE': self.get_input_value('__VIEWSTATE'),
        }
        return self._post_form(form_data, raw=True)


class AccountProcessor(object):
    LOGIN_URL = 'http://sfi.intermedica.com.br/'
    MAIN_URL = 'http://sfi.intermedica.com.br/Default.aspx?Contrato='

    def __init__(self, username, password):
        self._username = username
        self._password = password

        self._session = requests.Session()
        self._session.headers.update({
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'Host': 'sfi.intermedica.com.br',
            'Origin': 'http://sfi.intermedica.com.br',
            'Referer': 'http://sfi.intermedica.com.br/Default.aspx?Contrato=',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
        })

    def process(self):
        login_form = self._parse_login_form()
        main_page = self._login(login_form)

        self._parse_page(main_page)

    def _parse_page(self, page):
        form = FormParser(page)
        for contrato in form.get_contrato_options():
            self._parse_contrato(page, contrato)

    def _parse_contrato(self, page, contrato):
        logger.info('Contrato {}'.format(contrato.text))
        form = FormPoster(self._session, page, self.MAIN_URL)
        page = form.post_contrato(contrato)
        form = FormParser(page)

        for sub_contrato in form.get_sub_contrato_options():
            self._parse_sub_contrato(page, contrato, sub_contrato)

    def _parse_sub_contrato(self, page, contrato, sub_contrato):
        logger.info('*SubContrato {}'.format(sub_contrato.text))
        form = FormPoster(self._session, page, self.MAIN_URL)
        page = form.post_sub_contrato(sub_contrato)
        form = FormParser(page)

        for competencia in form.get_competencia_options():
            self._parse_competencia(page, contrato, sub_contrato, competencia)

    def _parse_competencia(self, page, contrato, sub_contrato, competencia):
        logger.info('**Competencia {}'.format(competencia.text))
        form = FormPoster(self._session, page, self.MAIN_URL)
        page = form.post_competencia(competencia)
        form = FormPoster(self._session, page, self.MAIN_URL)
        page = form.post_pesquisar()

        form = FormPoster(self._session, page, self.MAIN_URL)
        table_parser = TableParser(page)
        c = Collector(form, table_parser, self._username, contrato.text, sub_contrato.text, competencia.text)
        c.submit_arquivos_texto()
        c.submit_nota_fiscal()

    def _parse_login_form(self):
        soup = _soup(self._session.get(self.LOGIN_URL).content)
        return FormParser(soup)

    def _login(self, login_form):
        form_data = login_form.get_inputs()
        form_data['ctl00$ContentPlaceHolder1$txtCPF'] = self._username
        form_data['ctl00$ContentPlaceHolder1$txtSenha'] = self._password
        r = self._session.post(self.LOGIN_URL, data=form_data)
        r.raise_for_status()
        return _soup(r.content)


class Collector(object):
    def __init__(self, form_poster, table_parser, username, contrato, sub_contrato, competencia):
        self.username = username
        self.contrato = contrato
        self.sub_contrato = sub_contrato
        self.competencia = competencia

        self._form_poster = form_poster
        self._table_parser = table_parser

    def _download_document(self, content, name, url, form_poster):
            item_url = self._get_document_url(form_poster, url)
            if not item_url:
                return
            r = form_poster._session.get(item_url)
            r.raise_for_status()
            content[name] = r.content

    def _get_nota_fiscal_img_url(self, nota_fiscal_url):
        r = self._form_poster._session.get(nota_fiscal_url)
        soup = _soup(self._get_url_content(nota_fiscal_url))
        src = soup.find('form').find('img')['src']
        return urlparse.urljoin(r.url, src)

    def _get_document_url(self, button_name):
        if button_name is None:
            return None
        page = self._form_poster.post_download_document(button_name)
        for soup in page.find_all('script'):
            url = self._parse_script_for_url(soup, self._form_poster._url)
            if url is not None:
                return url
        raise ValueError('Doc page has no URL')

    def _parse_script_for_url(self, soup, url):
        for line in str(soup).split('\n'):
            if not line.startswith('abrir('):
                continue
            path = '/' + line.strip().lstrip("abrir('").rstrip("');//]]>")
            return urlparse.urljoin(url, path)
        return None

    def _download_ppv(self, button_name):
        if button_name is None:
            return None
        zip_folder = self._form_poster.post_download_document(button_name, raw=True)
        return zip_folder

    def _download_arquivos(self):
        r = self._form_poster.post_download_arquivos_texto()
        r.raise_for_status()
        zip_file = r.content
        parsed = urlparse.urlparse(r.request.url)
        filename = urlparse.parse_qs(parsed.query)['sZipDestino'][0]
        return filename, zip_file

    @staticmethod
    def _upload_file(desc, content, name, ext):
        file_id = sjutils.archive_file(StringIO(content), name, async=False)
        return {
            'sjid': file_id,
            'desc': desc,
            'name': name,
            'ext': ext,
        }

    def _get_url_content(self, url):
        r = self._form_poster._session.get(url)
        r.raise_for_status()
        return r.content

    @staticmethod
    def _parse_dt(dt):
        return parse(dt, default=datetime.datetime(2000, 1, 1))

    def submit_arquivos_texto(self):
        """
        Fields & Series ID - Download Arquivos Texto
        Source Fields:

        cpf -- unique user id
        contrato -- <contract code> - <contract name>
        subcontrato -- <subcontract code> - <subcontract name>
        competencia -- MM/YYYY of document
        source_obj.competencia_date -- competencia as a date field being the first day of the month
        Series ID: contrato\subcontrato\competencia
        """
        arquivos, arquivos_file = self._download_arquivos()
        sid = datastore.sid(self.contrato, self.sub_contrato, self.competencia)

        fields = {
            'cpf': self.username,
            'contrato': self.contrato,
            'subcontrato': self.sub_contrato,
            'competencia': self.competencia,
            'competencia_date': self._parse_dt(self.competencia).isoformat(),
        }
        docs_obj =  [
            self._upload_file('Arquivos Texto', arquivos_file, arquivos, 'zip'),
        ]

        datastore.put_fields(sid, {'source_obj': fields, 'docs_obj': docs_obj})

    def submit_nota_fiscal(self):
        for row in self._table_parser.rows:
            self._submit_nota_fiscal_item(row)

    def _submit_nota_fiscal_item(self, row):
        """
        Fields & Series ID - Nota Fiscal
        Source Fields:

        cpf -- unique user id
        contrato -- <contract code> - <contract name>
        subcontrato -- <subcontract code> - <subcontract name>
        competencia -- MM/YYYY of document
        nota_fiscal_id -- unique receipt ID from Intermédica
        emissao_date -- bill sent date
        vencimento_date - bill due date
        valor -- value of the bill in BRL
        Series ID: nota_fiscal_id
        """

        columns = row.get_fields()
        fields = {
            'cpf': self.username,
            'contrato': self.contrato,
            'subcontrato': self.sub_contrato,
            'competencia': self.competencia,
            'competencia_date': self._parse_dt(self.competencia).isoformat(),

            'nota_fiscal_id': columns['Nota Fiscal'],
            'emissao_date': self._parse_dt(columns['Emissao']).isoformat(),
            'vencimento_date': self._parse_dt(columns['Vencimento']).isoformat(),
            'valor': columns['Valor'],
        }

        docs_obj = []

        ppv_file = self._download_ppv(row.get_ppv_name())
        if ppv_file:
            docs_obj.append(self._upload_file('PPV', ppv_file, 'ppv.zip', 'zip'))

        boleto_url = self._get_document_url(row.get_boleto_name())
        if boleto_url:
            boleto_file = self._get_url_content(boleto_url)
            docs_obj.append(self._upload_file('Boleto', boleto_file, 'boleto.pdf', 'pdf'))

        descr_fatura_url = self._get_document_url(row.get_descr_fatura_name())
        if descr_fatura_url:
            descr_fatura_file = self._get_url_content(descr_fatura_url)
            docs_obj.append(self._upload_file('Descr. Fatura', descr_fatura_file, 'descr_fatura.pdf', 'pdf'))


        nota_fiscal_url = self._get_document_url(row.get_nota_fiscal_name())
        if nota_fiscal_url:
            nota_fiscal_img_url = self._get_nota_fiscal_img_url(nota_fiscal_url)
            nota_fiscal_file = self._get_url_content(nota_fiscal_img_url)
            docs_obj.append(self._upload_file('Nota Fiscal', nota_fiscal_file, 'nota_fiscal.gif', 'gif'))

        sid = datastore.sid(columns['Nota Fiscal'])
        datastore.put_fields(sid, {'source_obj': fields, 'docs_obj': docs_obj})


class RowParser(object):
    def __init__(self, fields, nota_fiscal, boleto, descr_fatura, ppv):
        self._fields = fields
        self._nota_fiscal = nota_fiscal
        self._boleto = boleto
        self._descr_fatura = descr_fatura
        self._ppv = ppv

    def get_fields(self):
        return {name: soup.text.strip('\n') for name, soup in self._fields}

    def get_ppv_name(self):
        return self._get_button_name(self._ppv)

    def get_nota_fiscal_name(self):
        return self._get_button_name(self._nota_fiscal)

    def get_boleto_name(self):
        return self._get_button_name(self._boleto)

    def get_descr_fatura_name(self):
        return self._get_button_name(self._descr_fatura)

    def _get_button_name(self, soup):
        soup = soup.find('input')
        if soup and not soup.has_attr('disabled'):
            return soup['name']
        return None


class TableParser(object):
    def __init__(self, soup):
        self.soup = soup.find('table', {'id': 'ContentPlaceHolder1_dgLista'})
        if self.soup is None:
            self.rows = []
            return

        expected_headers = ['SubContrato', 'Nota Fiscal', 'Emissao', 'Vencimento', 'Valor',
                'Nota Fiscal', 'Boleto', 'C.Credito', 'Descr. Fatura', 'PPV']

        actual_headers = []
        header_soup = self.soup.find('tr', {'class': 'titulos'})
        for it in header_soup.find_all('th'):
            name = ' '.join(it.text.split())
            actual_headers.append(name)

        if expected_headers != actual_headers:
            raise ValueError("Headers has changed, can't parse main table")

        self.rows = []
        for i, row in enumerate(self.soup.find_all('tr')):
            if i == 0:
                continue

            vals = row.find_all('td')

            named_values = zip(expected_headers, vals)
            fields = named_values[:5]
            nota_fiscal = named_values[5][1]
            boleto = named_values[6][1]
            descr_fatura = named_values[8][1]
            ppv = named_values[9][1]

            self.rows.append(RowParser(fields, nota_fiscal, boleto, descr_fatura, ppv))
